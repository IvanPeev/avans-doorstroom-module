<?php
namespace prod\controls;

use \php\error as ERROR;
use \php\controls\AbstractController as AbstractController;

class GuestController extends AbstractController {
	public function __construct($control, $action) {
		parent::__construct($control, $action);
		
		$userType = $this->model->getUserPermission();
		$this->view->set('userType', $userType);
	}

	// ------------ HOME PAGE ------------
	
	public function defaultAction() {
		$bandCount = $this->model->getBandCount();
		$albumCount = $this->model->getAlbumCount();
		$songCount = $this->model->getSongCount();
		$genreCount = $this->model->getGenreCount();

		$this->view->set('bandCount', $bandCount);
		$this->view->set('albumCount', $albumCount);
		$this->view->set('songCount', $songCount);
		$this->view->set('genreCount', $genreCount);
	}

	// ------------ BANDS PAGE ------------
	public function bandsAction() {
		$bands = $this->model->getBands();
		$this->view->set('bands', $bands);
	}

	// ------------ BAND PAGE ------------

	public function bandAction() {
		$band = $this->model->getBand();
		$bandGenres = $this->model->getBandGenreNames();
		$bandAlbums = $this->model->getBandAlbums();
		$this->view->set('band', $band);
		$this->view->set('bandGenres', $bandGenres);
		$this->view->set('bandAlbums', $bandAlbums);
	}

	// ------------ ALBUMS PAGE ------------

	public function albumsAction() {
		$albums = $this->model->getAlbums();
		$this->view->set('albums', $albums);
	}

	// ------------ ALBUM PAGE ------------

	public function albumAction() {
		$album = $this->model->getAlbum();
		$albumSongs = $this->model->getAlbumSongs();
		$this->view->set('album', $album);
		$this->view->set('albumSongs', $albumSongs);
	}

	// ------------ LOGIN PAGE ------------

	public function loginAction() {
		if (!$this->model->isPostEmpty()) {
			$this->login();
		}
	}

	private function login() {
		$resultLogin = $this->model->checkLogin();

		switch ($resultLogin) {
			case REQUEST_SUCCESS:
				$this->view->set("note", "Welcome ".$_SESSION['user']->getUsername());
				$permission = $this->model->getUserPermission();
				$this->forward("default", $permission);
				break;
			case REQUEST_FAILURE_DATA_INVALID:
				$this->view->set("note", "User data invalid, try again");
				break;
			case REQUEST_FAILURE_DATA_INCOMPLETE:
				$this->view->set("note", "Username and or password are not filled in");
				break;
		}
	}
}
