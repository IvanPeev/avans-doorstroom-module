<?php
namespace prod\controls;

use \php\error as ERROR;
use \php\controls\AbstractController as AbstractController;

class ManagerController extends AbstractController {
	public function __construct($control, $action) {
		parent::__construct($control, $action);
		
		$userType = $this->model->getUserPermission();
		$this->view->set('userType', $userType);
	}

	// ------------ HOME PAGE ------------

	public function defaultAction() {
		$bandCount = $this->model->getBandCount();
		$albumCount = $this->model->getAlbumCount();
		$songCount = $this->model->getSongCount();
		$genreCount = $this->model->getGenreCount();

		$this->view->set('bandCount', $bandCount);
		$this->view->set('albumCount', $albumCount);
		$this->view->set('songCount', $songCount);
		$this->view->set('genreCount', $genreCount);
	}

	// ------------ BANDS PAGE ------------

	public function bandsAction() {
		$bands = $this->model->getBands();
		$this->view->set('bands', $bands);
	}

	// ------------ BAND PAGE ------------

	public function bandAction() {
		$band = $this->model->getBand();
		$bandGenres = $this->model->getBandGenreNames();
		$bandAlbums = $this->model->getBandAlbums();
		$this->view->set('band', $band);
		$this->view->set('bandGenres', $bandGenres);
		$this->view->set('bandAlbums', $bandAlbums);
	}

	// ------------ BAND ADD PAGE ------------

	public function bandAddAction() {
		$genres = $this->model->getGenres();
		$this->view->set('genres', $genres);

		if (!$this->model->isPostEmpty()) {
			$this->model->bandAdd();
			$this->forward('bands', $userType);
		}
	}

	// ------------ BAND EDIT PAGE ------------

	public function bandEditAction() {
		$genres = $this->model->getGenres();
		$band = $this->model->getBand();
		$bandGenres = $this->model->getBandGenres();
		$this->view->set('genres', $genres);
		$this->view->set('band', $band);
		$this->view->set('bandGenres', $bandGenres);

		if (!$this->model->isPostEmpty()) {
			$this->model->bandEdit();
			$this->forward('bands', $userType);
		}
	}

	// ------------ BAND DELETE ------------

	public function bandDeleteAction() {
		$this->model->bandDelete();
		$this->forward('bands', $userType);
	}

	// ------------ BAND ALBUM ADD PAGE ------------

	public function bandAlbumAddAction() {
		if (!$this->model->isPostEmpty()) {
			$this->model->bandAlbumAdd();
			$this->forward('bands', $userType);
		}
	}

	// ------------ BAND ALBUM EDIT PAGE ------------

	public function bandAlbumEditAction() {
		$album = $this->model->getAlbum();
		$this->view->set('album', $album);

		if (!$this->model->isPostEmpty()) {
			$this->model->bandAlbumEdit();
			$this->forward('bands', $userType);
		}
	}

	// ------------ ALBUMS PAGE ------------

	public function albumsAction() {
		$albums = $this->model->getAlbums();
		$this->view->set('albums', $albums);
	}

	// ------------ ALBUM PAGE ------------

	public function albumAction() {
		$album = $this->model->getAlbum();
		$albumSongs = $this->model->getAlbumSongs();
		$this->view->set('album', $album);
		$this->view->set('albumSongs', $albumSongs);
	}

	// ------------ ALBUM ADD PAGE ------------
	public function albumAddAction() {
		$bands = $this->model->getBands();
		$this->view->set('bands', $bands);

		if (!$this->model->isPostEmpty()) {
			$this->model->albumAdd();
			$this->forward('albums', $userType);
		}
	}

	// ------------ ALBUM EDIT PAGE ------------

	public function albumEditAction() {
		$album = $this->model->getAlbum();
		$bands = $this->model->getBands();
		$this->view->set('album', $album);
		$this->view->set('bands', $bands);

		if (!$this->model->isPostEmpty()) {
			$this->model->albumEdit();
			$this->forward('albums', $userType);
		}
	}

	// ------------ ALBUM DELETE ------------

	public function albumDeleteAction() {
		$this->model->albumDelete();
		$this->forward('albums', $userType);
	}

	// ------------ SONG ADD ------------

	public function songAddAction() {
		if (!$this->model->isPostEmpty()) {
			$this->model->songAdd();
			$this->forward('albums', $userType);
		}
	}

	// ------------ SONG EDIT ------------

	public function songEditAction() {
		$song = $this->model->getSong();
		$this->view->set('song', $song);



		if (!$this->model->isPostEmpty()) {
			$this->model->songEdit();
			$this->forward('albums', $userType);
		}
	}

	// ------------ SONG DELETE ------------

	public function songDeleteAction() {
		$this->model->songDelete();
		$this->forward('albums', $userType);
	}

	// ------------ GENRES PAGE ------------

	public function genresAction() {
		$genres = $this->model->getGenres();
		$this->view->set('genres', $genres);
	}

	// ------------ GENRE ADD PAGE ------------

	public function genreAddAction() {
		if (!$this->model->isPostEmpty()) {
			$this->model->genreAdd();
			$this->forward('genres', $userType);
		}
	}

	// ------------ GENRE EDIT PAGE ------------

	public function genreEditAction() {
		$genre = $this->model->getGenre();
		$this->view->set('genre', $genre);

		if (!$this->model->isPostEmpty()) {
			$this->model->genreEdit();
			$this->forward('genres', $userType);
		}
	}

	// ------------ GENRE DELETE ------------

	public function genreDeleteAction() {
		$this->model->genreDelete();
		$this->forward('genres', $userType);
	}
}
