<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">All albums</h1>
	<table>
		<thead>
			<th>Name</th>
			<th>Published</th>
			<th>Copies</th>
			<th colspan="2">Actions</th>
		</thead>
		<tbody>
			<?php foreach ($albums as $album): ?>
				<tr>
					<td>
						<a href="?control=manager&action=album&id=<?= $album->getId(); ?>"><?= $album->getName(); ?></a>
					</td>
					<td><?= $album->getPublished(); ?></td>
					<td><?= $album->getCopies(); ?></td>
					<td>
						<a href="?control=manager&action=albumEdit&id=<?= $album->getId(); ?>">Edit</a>
					</td>
					<td>
						<a href="?control=manager&action=albumDelete&id=<?= $album->getId(); ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br/>
	<a href="?control=manager&action=albumAdd">Add album</a>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
