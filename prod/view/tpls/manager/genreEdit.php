<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Edit genre</h1>
	<form method="post">
		<label for="genre">Genre</label>
		<br/>
		<input type="text" placeholder="Genre name" name="genre" value="<?= !empty($genre->getName()) ? $genre->getName() : '' ?>" required/>
		<br/>
		<br/>
		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
