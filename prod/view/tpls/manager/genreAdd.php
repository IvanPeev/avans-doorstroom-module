<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Add a new genre</h1>
	<form method="post">
		<label for="genre">Genre</label>
		<br/>
		<input type="text" placeholder="Genre" name="genre" required/>
		<br/>
		<br/>
		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
