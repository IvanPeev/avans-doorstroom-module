<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized"><?= $album->getName(); ?> (<?= $album->getPublished(); ?>)</h1>
	<p>
		<span class="highlighted">
			Description:
		</span>
		<?= !empty($album->getDescription()) ? $album->getDescription() : 'N/A'; ?>
	</p>
	<p>
		<span class="highlighted">Copies sold:</span> 
		<?= $album->getCopies(); ?>
	</p>
	<p>
		<span class="highlighted">Songs:</span>
	</p>

	<?php
		if (count($albumSongs) === 0) {
			echo "No songs available";
		} else {
	?>
	
	<table>
		<thead>
			<th>Name</th>
			<th>Duration</th>
			<th colspan="2">Actions</th>
		</thead>
		<tbody>
			<?php foreach ($albumSongs as $albumSong): ?>
				<tr>
					<td><?= $albumSong->getName(); ?></td>
					<td><?= $albumSong->getDuration(); ?></td>
					<td>
						<a href="?control=manager&action=songEdit&id=<?= $albumSong->getId(); ?>">Edit</a>
					</td>
					<td>
						<a href="?control=manager&action=songDelete&id=<?= $albumSong->getId(); ?>&albumId=<?= $album->getId(); ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<?php
		// close else-statement
		}
	?>
	<br/>
	<br/>
	<a href="?control=manager&action=songAdd&albumId=<?= $album->getId(); ?>">Add song</a>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
