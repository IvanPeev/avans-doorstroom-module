<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Add a new band</h1>
	<form method="post">
		<label for="name">Name</label>
		<br/>
		<input type="text" placeholder="Name" name="name" required/>
		<br/>
		<br/>

		<label for="origin">Origin</label>
		<br/>
		<input type="text" placeholder="Origin" name="origin" required/>
		<br/>
		<br/>

		<label for="founded">Founded</label>
		<br/>
		<input type="text" placeholder="Founded" name="founded" required/>
		<br/>
		<br/>

		<label for="active">Active</label>
		<br/>
		<select name="active" required>
			<option value="1">Yes</option>
			<option value="0">No</option>
		</select>
		<br/>
		<br/>

		<label for="genres[]">Genres</label>
		<br/>
		<?php foreach ($genres as $genre): ?>
			<input type="checkbox" name="genres[]" value="<?= $genre->getId(); ?>" />
			<label><?= $genre->getName(); ?></label>
			<br/>
		<?php endforeach ?>
		<br/>

		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
