<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Bands page</h1>
	<table>
		<thead>
			<th>Name</th>
			<th>Origin</th>
			<th>Founded</th>
			<th>Active</th>
			<th colspan="2">Actions</th>
		</thead>
		<tbody>
			<?php foreach ($bands as $band): ?>
				<tr>
					<td>
						<a href="?control=manager&action=band&id=<?= $band->getId(); ?>"><?= $band->getName(); ?></a>
					</td>
					<td><?= $band->getOrigin(); ?></td>
					<td><?= $band->getFounded(); ?></td>
					<td><?= $band->getActive() ? "Yes" : "No"; ?></td>
					<td>
						<a href="?control=manager&action=bandEdit&id=<?= $band->getId(); ?>">Edit</a>
					</td>
					<td>
						<a href="?control=manager&action=bandDelete&id=<?= $band->getId(); ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br/>
	<a href="?control=manager&action=bandAdd">Add band</a>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
