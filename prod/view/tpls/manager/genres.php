<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Genres</h1>
	<p>Here you can view all the different types of genres that are assignable to a band.</p>
	<table>
		<thead>
			<th>Name</th>
			<th colspan="2">Actions</th>
		</thead>
		<tbody>
			<?php foreach ($genres as $genre): ?>
				<tr>
					<td><?= $genre->getName(); ?></td>
					<td>
						<a href="?control=manager&action=genreEdit&id=<?= $genre->getId(); ?>">Edit</a>
					</td>
					<td>
						<a href="?control=manager&action=genreDelete&id=<?= $genre->getId(); ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br/>
	<a href="?control=manager&action=genreAdd">Add genre</a>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
