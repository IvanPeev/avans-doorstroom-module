<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Edit album</h1>
	<form method="post">
		<label for="name">Name</label>
		<br/>
		<input type="text" placeholder="Name" name="name" value="<?= !empty($album->getName()) ? $album->getName() : ''; ?>" required/>
		<br/>
		<br/>
		
		<label for="description">Description</label>
		<br/>
		<textarea class="description" placeholder="Description" name="description"><?= !empty($album->getDescription()) ? $album->getDescription() : ''; ?></textarea>
		<br/>
		<br/>
		
		<label for="published">Year published</label>
		<br/>
		<input type="text" placeholder="Year published" name="published" value="<?= !empty($album->getPublished()) ? $album->getPublished() : ''; ?>" requried/>
		<br/>
		<br/>
		
		<label for="copies">Copies sold</label>
		<br/>
		<input type="text" placeholder="Copies sold" name="copies" value="<?= !empty($album->getCopies()) ? $album->getCopies() : ''; ?>" required/>
		<br/>
		<br/>

		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
