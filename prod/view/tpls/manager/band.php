<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized"><?= $band->getName(); ?> (<?= $band->getFounded(); ?>)</h1>
	<p>
		<span class="highlighted">Origin:</span> 
		<?= $band->getOrigin(); ?>
	</p>
	<p>
		<span class="highlighted">Active:</span> 
		<?= $band->getActive() ? "Yes" : "No"; ?>
	</p>
	<p>
		<span class="highlighted">Genres: </span>
		<?php
			if (count($bandGenres) > 0) {
				$genres = '';
				for ($i = 0; $i < count($bandGenres); $i++) {
					$genres .= $bandGenres[$i]->getName();

					if ($i < count($bandGenres) - 1) {
						$genres .= ', ';
					}
				}
				echo $genres;
			} else {
				echo "N/A";
			}
		?>
	</p>

	<p class="highlighted">Albums:</p>
	<table>
		<thead>
			<th>Name</th>
			<th>Published</th>
			<th>Copies</th>
			<th colspan="2">Actions</th>
		</thead>
		<tbody>
			<?php foreach ($bandAlbums as $bandAlbum): ?>
				<tr>
					<td>
						<a href='?control=manager&action=album&id=<?= $bandAlbum->getId(); ?>'><?= $bandAlbum->getName(); ?></a>
					</td>
					<td><?= $bandAlbum->getPublished(); ?></td>
					<td><?= $bandAlbum->getCopies(); ?></td>
					<td>
						<a href="?control=manager&action=bandAlbumEdit&id=<?= $bandAlbum->getId(); ?>">Edit</a>
					</td>
					<td>
						<a href="?control=manager&action=albumDelete&id=<?= $bandAlbum->getId(); ?>">Delete</a>
					</td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<br/>
	<a href="?control=manager&action=bandAlbumAdd&bandId=<?= $band->getId(); ?>">Add album</a>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
