<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Add a new song</h1>
	<form method="post">
		<label for="name">Name</label>
		<br/>
		<input type="text" placeholder="Name" name="name" required/>
		<br/>
		<br/>
		
		<label for="duration">Duration</label>
		<br/>
		<input type="text" placeholder="Duration" name="duration" required/>
		<br/>
		<br/>
		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
