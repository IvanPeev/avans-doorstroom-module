<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Add a new band</h1>
	<form method="post">
		<label for="name">Name</label>
		<br/>
		<input type="text" placeholder="Name" name="name" value="<?= !empty($band->getName()) ? $band->getName() : ''; ?>" required/>
		<br/>
		<br/>

		<label for="origin">Origin</label>
		<br/>
		<input type="text" placeholder="Origin" name="origin" value="<?= !empty($band->getOrigin()) ? $band->getOrigin() : ''; ?>" required/>
		<br/>
		<br/>

		<label for="founded">Founded</label>
		<br/>
		<input type="text" placeholder="Founded" name="founded" value="<?= !empty($band->getFounded()) ? $band->getFounded() : ''; ?>" required/>
		<br/>
		<br/>

		<label for="active">Active</label>
		<br/>
		<select name="active" required>
			<option value="1" <?= $band->getActive() === '1' ? 'selected' : '' ?>>Yes</option>
			<option value="0" <?= $band->getActive() === '0' ? 'selected' : '' ?>>No</option>
		</select>
		<br/>
		<br/>

		<label for="genres[]">Genres</label>
		<br/>

		<?php
			$checked = array_fill(0, count($genres), false);
			for ($i = 0; $i < count($genres); $i++) {
				$checked[$i] = false;

				foreach ($bandGenres as $bandGenre) {
					if ($genres[$i]->getId() === $bandGenre->getGenreId()) {
						$checked[$i] = true;
					}
				}

				echo '<input type="checkbox" name="genres[]" value="'.$genres[$i]->getId().'" '.($checked[$i] ? 'checked' : '').'/>';
				echo '<label>'.$genres[$i]->getName().'</label><br/>';
			}
		?>

		<br/>
		<input type="submit" value="Submit"/>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
