<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized"><?= $album->getName(); ?> (<?= $album->getPublished(); ?>)</h1>
	<p>
		<span class="highlighted">
			Description:
		</span>
		<?= !empty($album->getDescription()) ? $album->getDescription() : 'N/A'; ?>
	</p>
	<p>
		<span class="highlighted">Copies sold:</span> 
		<?= $album->getCopies(); ?>
	</p>
	<p>
		<span class="highlighted">Songs:</span>
	</p>

	<?php
		if (count($albumSongs) === 0) {
			echo "No songs available";
		} else {
	?>
	
	<table>
		<thead>
			<th>Name</th>
			<th>Duration</th>
		</thead>
		<tbody>
			<?php foreach ($albumSongs as $albumSong): ?>
				<tr>
					<td><?= $albumSong->getName(); ?></td>
					<td><?= $albumSong->getDuration(); ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
	<?php
		// close else-statement
		}
	?>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
