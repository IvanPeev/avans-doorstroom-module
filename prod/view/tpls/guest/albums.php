<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">All Albums</h1>
	<table>
		<thead>
			<th>Name</th>
			<th>Published</th>
			<th>Copies</th>
		</thead>
		<tbody>
			<?php foreach ($albums as $album): ?>
				<tr>
					<td>
						<a href='?control=guest&action=album&id=<?= $album->getId(); ?>'><?= $album->getName(); ?></a>
					</td>
					<td><?= $album->getPublished(); ?></td>
					<td><?= $album->getCopies(); ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
