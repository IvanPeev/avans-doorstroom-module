<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">All Bands</h1>
	<table>
		<thead>
			<th>Name</th>
			<th>Origin</th>
			<th>Founded</th>
			<th>Active</th>
		</thead>
		<tbody>
			<?php foreach ($bands as $band): ?>
				<tr>
					<td>
						<a href="?control=guest&action=band&id=<?= $band->getId(); ?>"><?= $band->getName(); ?></a>
					</td>
					<td><?= $band->getOrigin(); ?></td>
					<td><?= $band->getFounded(); ?></td>
					<td><?= $band->getActive() ? "Yes" : "No"; ?></td>
				</tr>
			<?php endforeach ?>
		</tbody>
	</table>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
