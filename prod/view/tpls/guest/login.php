<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Login page</h1>
	<form method="POST" autocomplete="off">
		<table>
			<tr>
				<td>
					<input type="text" autocomplete="off" placeholder="Username" name="username" required/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="password" autocomplete="off" placeholder="Password" name="password" required/>
				</td>
			</tr>
			<tr>
				<td>
					<input type="submit" value="Log in"/>
				</td>
			</tr>
		</table>
	</form>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
