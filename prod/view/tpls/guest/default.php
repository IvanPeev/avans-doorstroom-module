<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/header.php";
?>

<div>
	<h1 class="metallized">Metal collection</h1>
	<p>Welcome to the music collection website. Here we collect all the cool metal music we can find!</p>
	<p>You can view all the <a href="?control=guest&action=bands">bands</a> and <a href="?control=guest&action=albums">albums</a></p>
	<p>These are the current statistics:</p>
	<table>
		<thead>
			<th>Bands</th>
			<th>Albums</th>
			<th>Songs</th>
			<th>Genres</th>
		</thead>
		<tbody>
			<tr>
				<td><?= $bandCount; ?></td>
				<td><?= $albumCount; ?></td>
				<td><?= $songCount; ?></td>
				<td><?= $genreCount; ?></td>
			</tr>
		</tbody>
	</table>
</div>

<?php
	include str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE)."view/tpls/include/footer.php";
?>
