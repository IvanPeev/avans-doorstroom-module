<html>
	<head>
		<title>Test website</title>
		<meta charset="utf-8"/>
		<link href="css/main.css" rel="stylesheet"/>
	</head>
	<body>
		<div>
			<nav class="header">
				<?php
					echo '<a href="?control='.$userType.'&action=default">Home</a>';
					echo '<a href="?control='.$userType.'&action=bands">Bands</a>';
					echo '<a href="?control='.$userType.'&action=albums">Albums</a>';
					if ($userType === 'manager') {
						echo '<a href="?control='.$userType.'&action=genres">Genres</a>';
						echo '<a href="?control='.$userType.'&action=logout">Logout</a>';
					} else {
						echo "<a href='?control=guest&action=login'>Login</a>";
					}
				?>
			</nav>
		</div>
