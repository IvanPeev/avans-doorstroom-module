<?php
namespace prod\models;
use \php\models\AbstractModel as AbstractModel;

class GuestModel extends AbstractModel {
	public function __construct($control, $action) {
		parent::__construct($control, $action);
	}

	// ------------ DEFAULT PAGE FUNCTIONALITY ------------

	public function getBandCount() {
		$sql = "SELECT * from bands";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band'));
	}

	public function getAlbumCount() {
		$sql = "SELECT * FROM albums";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album'));
	}

	public function getSongCount() {
		$sql = "SELECT * FROM songs";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Song'));
	}

	public function getGenreCount() {
		$sql = "SELECT * FROM genres";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre'));
	}

	// ------------ BANDS PAGE FUNCTIONALITY ------------

	public function getBands() {
		$sql = "SELECT * FROM bands";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band');
	}

	// ------------ BAND PAGE FUNCTIONALITY ------------

	public function getBand() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM bands WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band')[0];
	}

	public function getBandGenreNames() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT g.name
			FROM band_genres AS bg
			INNER JOIN bands AS b
			ON bg.bandId = b.id
			INNER JOIN genres AS g
			ON bg.genreId = g.id
			WHERE b.id = :id
		";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre');
	}

	public function getBandAlbums() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM albums WHERE bandId = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album');
	}

	// ------------ ALBUMS PAGE FUNCTIONALITY ------------

	public function getAlbums() {
		$sql = "SELECT * FROM albums";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();
		
		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album');
	}

	// ------------ ALBUM PAGE FUNCTIONALITY ------------

	public function getAlbum() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM albums WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album')[0];
	}

	public function getAlbumSongs() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM songs WHERE albumId = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Song');
	}

	// ------------ LOGIN PAGE FUNCTIONALITY ------------

	public function checkLogin() {
		$username = filter_input(INPUT_POST, 'username');
		$password = filter_input(INPUT_POST, 'password');

		if (($username !== null) && ($password !== null)) {
			$sql = "SELECT * FROM users WHERE username = :username AND password = :password";
			$sth = $this->dbh->prepare($sql);
			$sth->bindParam(':username', $username);
			$sth->bindParam(':password', $password);
			$sth->execute();
			$result = $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\User');

			if (count($result) === 1) {
				$this->startSession();
				$_SESSION['user'] = $result[0];
				return REQUEST_SUCCESS;
			}
			return REQUEST_FAILURE_DATA_INVALID;
		}
		return REQUEST_FAILURE_DATA_INCOMPLETE;
	}
}
