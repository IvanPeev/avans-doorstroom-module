<?php
namespace prod\models\db;
use \php\models\db\Entity as Entity;

class Song extends Entity {
	protected $id;
	protected $name;
	protected $duration;
	protected $albumId;
}
