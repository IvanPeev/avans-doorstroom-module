<?php
namespace prod\models\db;
use \php\models\db\Entity as Entity;

class Band extends Entity {
	protected $id;
	protected $name;
	protected $origin;
	protected $founded;
	protected $active;
}
