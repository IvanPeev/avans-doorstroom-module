<?php
namespace prod\models\db;
use \php\models\db\Entity as Entity;

class Album extends Entity {
	protected $id;
	protected $name;
	protected $description;
	protected $published;
	protected $copies;
	protected $bandId;
}
