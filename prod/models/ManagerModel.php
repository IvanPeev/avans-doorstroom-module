<?php
namespace prod\models;
use \php\models\AbstractModel as AbstractModel;

class ManagerModel extends AbstractModel {
	public function __construct($control, $action) {
		parent::__construct($control, $action);
	}

	// ------------ DEFAULT PAGE FUNCTIONALITY ------------

	public function getBandCount() {
		$sql = "SELECT * from bands";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band'));
	}

	public function getAlbumCount() {
		$sql = "SELECT * FROM albums";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album'));
	}

	public function getSongCount() {
		$sql = "SELECT * FROM songs";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Song'));
	}

	public function getGenreCount() {
		$sql = "SELECT * FROM genres";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return count($sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre'));
	}

	// ------------ BANDS PAGE FUNCTIONALITY ------------

	public function getBands() {
		$sql = "SELECT * FROM bands";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band');
	}

	// ------------ BAND (GET / NEW / EDIT / DELETE) FUNCTIONALITY ------------

	public function getBand() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM bands WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band')[0];
	}

	public function getBandGenres() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM band_genres WHERE bandId = :bandId";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':bandId', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\BandGenre');
	}

	public function getBandGenreNames() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT g.name
			FROM band_genres AS bg
			INNER JOIN bands AS b
			ON bg.bandId = b.id
			INNER JOIN genres AS g
			ON bg.genreId = g.id
			WHERE b.id = :id
		";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre');
	}

	public function getBandAlbums() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM albums WHERE bandId = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album');
	}

	public function bandAdd() {
		$name = filter_input(INPUT_POST, 'name');
		$origin = filter_input(INPUT_POST, 'origin');
		$founded = filter_input(INPUT_POST, 'founded');
		$active = filter_input(INPUT_POST, 'active');
		$genres = filter_input(INPUT_POST, 'genres', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

		$sql = "INSERT INTO `bands` (`name`, `origin`, `founded`, `active`)
			VALUES (:name, :origin, :founded, :active)
		";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':origin', $origin);
		$sth->bindParam(':founded', $founded);
		$sth->bindParam(':active', $active);

		try {
			$sth->execute();
			$sth->closeCursor();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			if ($genres !== NULL) {
				$bandSql = "SELECT LAST_INSERT_ID() AS id";
				$bandSth = $this->dbh->prepare($bandSql);
				$bandSth->execute();

				$bandId = $bandSth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Band')[0]->getId();

				foreach ($genres as $genre) {
					$bandGenreSQL = "INSERT INTO band_genres (`bandId`, `genreId`) VALUES (:bandId, :genreId)";
					$bandGenreSth = $this->dbh->prepare($bandGenreSQL);
					$bandGenreSth->bindParam(':bandId', $bandId);
					$bandGenreSth->bindParam(':genreId', $genre);
					$bandGenreSth->execute();
				}
			}

			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function bandEdit() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$name = filter_input(INPUT_POST, 'name');
		$origin = filter_input(INPUT_POST, 'origin');
		$founded = filter_input(INPUT_POST, 'founded');
		$active = filter_input(INPUT_POST, 'active');
		$genres = filter_input(INPUT_POST, 'genres', FILTER_DEFAULT, FILTER_REQUIRE_ARRAY);

		$sql = "UPDATE bands
			SET `name` = :name, `origin` = :origin, `founded` = :founded, `active` = :active
			WHERE id = :id
		";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':origin', $origin);
		$sth->bindParam(':founded', $founded);
		$sth->bindParam(':active', $active);
		$sth->bindParam(':id', $id);

		try {
			$sth->execute();
			$sth->closeCursor();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		$bandGenreDeleteSQL = "DELETE FROM band_genres WHERE bandId = :bandId";
		$bandGenreDeleteSth = $this->dbh->prepare($bandGenreDeleteSQL);
		$bandGenreDeleteSth->bindParam(':bandId', $id);
		$bandGenreDeleteSth->execute();

		if ($genres !== NULL) {
			foreach ($genres as $genre) {
				$bandGenreSQL = "INSERT INTO band_genres (`bandId`, `genreId`) VALUES (:bandId, :genreId)";
				$bandGenreSth = $this->dbh->prepare($bandGenreSQL);
				$bandGenreSth->bindParam(':bandId', $id);
				$bandGenreSth->bindParam(':genreId', $genre);
				$bandGenreSth->execute();
			}
		}

		return REQUEST_SUCCESS;
	}

	public function bandDelete() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "DELETE FROM bands WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function bandAlbumAdd() {
		$bandId = filter_input(INPUT_GET, 'bandId', FILTER_VALIDATE_INT);
		$name = filter_input(INPUT_POST, 'name');
		$description = filter_input(INPUT_POST, 'description');
		$published = filter_input(INPUT_POST, 'published');
		$copies = filter_input(INPUT_POST, 'copies');
		$sql = "INSERT INTO `albums` (`name`, `description`, `published`, `copies`, `bandId`) VALUES (:name, :description, :published, :copies, :bandId)";

		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':description', $description);
		$sth->bindParam(':published', $published);
		$sth->bindParam(':copies', $copies);
		$sth->bindParam(':bandId', $bandId);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function bandAlbumEdit() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$bandId = filter_input(INPUT_POST, 'band');
		$name = filter_input(INPUT_POST, 'name');
		$description = filter_input(INPUT_POST, 'description');
		$published = filter_input(INPUT_POST, 'published');
		$copies = filter_input(INPUT_POST, 'copies');
		$sql = "UPDATE albums
						SET `name` = :name, `description` = :description, `published` = :published, `copies` = :copies
						WHERE id = :id";

		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':description', $description);
		$sth->bindParam(':published', $published);
		$sth->bindParam(':copies', $copies);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	// ------------ ALBUMS PAGE FUNCTIONALITY ------------

	public function getAlbums() {
		$sql = "SELECT * FROM albums";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album');
	}

	// ------------ ALBUM (GET / NEW / EDIT / DELETE) FUNCTIONALITY ------------

	public function getAlbum() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM albums WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Album')[0];
	}

	public function albumAdd() {
		$bandId = filter_input(INPUT_POST, 'band');
		$name = filter_input(INPUT_POST, 'name');
		$description = filter_input(INPUT_POST, 'description');
		$published = filter_input(INPUT_POST, 'published');
		$copies = filter_input(INPUT_POST, 'copies');
		$sql = "INSERT INTO `albums` (`name`, `description`, `published`, `copies`, `bandId`) VALUES (:name, :description, :published, :copies, :bandId)";

		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':description', $description);
		$sth->bindParam(':published', $published);
		$sth->bindParam(':copies', $copies);
		$sth->bindParam(':bandId', $bandId);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function albumEdit() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$bandId = filter_input(INPUT_POST, 'band');
		$name = filter_input(INPUT_POST, 'name');
		$description = filter_input(INPUT_POST, 'description');
		$published = filter_input(INPUT_POST, 'published');
		$copies = filter_input(INPUT_POST, 'copies');
		$sql = "UPDATE albums
						SET `name` = :name, `description` = :description, `published` = :published, `copies` = :copies, `bandId` = :bandId
						WHERE id = :id";

		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':description', $description);
		$sth->bindParam(':published', $published);
		$sth->bindParam(':copies', $copies);
		$sth->bindParam(':bandId', $bandId);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function albumDelete() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "DELETE FROM albums WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();

		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	// ------------ SONGS (GET / NEW / EDIT / DELETE) ------------
	
	public function getAlbumSongs() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM songs WHERE albumId = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Song');
	}

	public function getSong() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM songs WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Song')[0];
	}

	public function songAdd() {
		$albumId = filter_input(INPUT_GET, 'albumId', FILTER_VALIDATE_INT);
		$name = filter_input(INPUT_POST, 'name');
		$duration = filter_input(INPUT_POST, 'duration');

		$sql = "INSERT INTO `songs` (`name`, `duration`, `albumId`) VALUES (:name, :duration, :albumId)";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':duration', $duration);
		$sth->bindParam(':albumId', $albumId);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function songEdit() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$name = filter_input(INPUT_POST, 'name');
		$duration = filter_input(INPUT_POST, 'duration');

		$sql = "UPDATE songs
						SET `name` = :name, `duration` = :duration
						WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':name', $name);
		$sth->bindParam(':duration', $duration);
		$sth->bindParam(':id', $id);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function songDelete() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "DELETE FROM songs WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		
		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	// ------------ GENRES PAGE FUNCTIONALITY ------------

	public function getGenres() {
		$sql = "SELECT * FROM genres";
		$sth = $this->dbh->prepare($sql);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre');
	}

	// ------------ GENRE (GET / NEW / EDIT / DELETE) FUNCTIONALITY ------------

	public function getGenre() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "SELECT * FROM genres WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->execute();

		return $sth->fetchAll(\PDO::FETCH_CLASS, __NAMESPACE__.'\db\Genre')[0];
	}

	public function genreAdd() {
		$genre = filter_input(INPUT_POST, 'genre');

		$sql = "INSERT INTO `genres` (`name`) VALUES (:genre)";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':genre', $genre);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function genreEdit() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);
		$genre = filter_input(INPUT_POST, 'genre');
		$sql = "UPDATE genres
						SET `name` = :genre
						WHERE id = :id";
		
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);
		$sth->bindParam(':genre', $genre);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}

	public function genreDelete() {
		$id = filter_input(INPUT_GET, 'id', FILTER_VALIDATE_INT);

		$sql = "DELETE FROM genres WHERE id = :id";
		$sth = $this->dbh->prepare($sql);
		$sth->bindParam(':id', $id);

		try {
			$sth->execute();
		} catch (PDOException $e) {
			echo "<pre>".$e."</pre>";
			return REQUEST_FAILURE_DATA_INVALID;
		}

		$amountChanged = $sth->rowCount();
	
		if ($amountChanged === 1) {
			return REQUEST_SUCCESS;
		}
		return REQUEST_NOTHING_CHANGED;
	}
}
