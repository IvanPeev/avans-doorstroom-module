<?php
include "php/config.php";

function autoloader($className) {
	$class = str_replace('\\', DIRECTORY_SEPARATOR, $className);
	$file = "$class.php";

	if (file_exists($file)) {
		include $file;
	} else {
		echo "[ERROR]: $file does not exist<br/>";
	}
}

spl_autoload_register('autoloader');

$control = \filter_input(\INPUT_GET, 'control') ? : 'guest';
$action = \filter_input(\INPUT_GET, 'action') ? : 'default';

try {
	$dispatcher = new \php\controls\ControlDispatcher($control, $action);
	$dispatcher->dispatch();
}

catch (php\error\FrameworkException $e) {
	$error = $e->getMessage();
	echo $error;
}

catch (\PDOException $e) {
	$error = "Something went wrong in the database, notify the maintainer";
	echo $e->getMessage();
}

catch (\Exception $e) {
	$error = "An exception has occurred, check the code.";
	echo $error;
}
