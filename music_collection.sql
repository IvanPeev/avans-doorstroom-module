-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Dec 02, 2021 at 09:46 PM
-- Server version: 10.6.5-MariaDB
-- PHP Version: 8.0.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `music_collection`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(10000) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `published` int(5) NOT NULL,
  `copies` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bandId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`id`, `name`, `description`, `published`, `copies`, `bandId`) VALUES
(1, 'Rust in Peace', 'Rust in Peace is the fourth studio album by American heavy metal band Megadeth, released on September 24, 1990 by Capitol Records. It has been mentioned as one of the best thrash metal records of all time by publications such as Decibel and Kerrang!, and listed in the reference book 1001 Albums You Must Hear Before You Die. The album was nominated for a Grammy Award for Best Metal Performance at the 33rd Grammy Awards. At the 1991 Foundations Forum, the album received a Concrete Foundations Award for \"Top Radio Album\" and the single \"Hangar 18\" won \"Top Radio Cut\" award.', 1990, '1,000,000', 1),
(2, 'Red Fang', 'On their self-titled debut, Red Fang don\'t waste any time establishing themselves as a band that destroys amps, beers, and eardrums in equal measure. With a sound rooted in the down-and-dirty sound of classic metal and stoner rock, the band is like a hyper-energized Mountain, tearing through fuzzed-out Southern rock-style riffs song after song without ever giving the dust they kick up time to settle. With no studio trickery or conceptual weirdness at work, the album is a refreshing island in a sea of increasingly heady and convoluted heavy metal. Red Fang shows that with the right combination of pacing, volume, and swagger, the rest just kind of falls into place.', 2009, '200,000', 2),
(3, 'Gravity X', 'Gravity X is the debut studio album by Swedish rock band Truckfighters, released on 12 July 2005 by Fuzzorama Records. The album was reissued alongside their subsequent album, Phi, in a compilation LP release in June 2013.', 2005, '150,000', 3),
(4, 'Crazy World', 'Crazy World is the eleventh studio album by the German heavy metal band Scorpions, released on 6 November 1990. The album peaked at No. 21 on the Billboard 200 chart for albums in 1991. That same year, the song \"Wind of Change\" reached No. 4 on the Billboard Hot 100 and \"Send Me an Angel\" reached No. 44 on the same chart. It also has the only Scorpions track to credit bassist Francis Buchholz as a writer, \"Kicks After Six\". This album was the band\'s first album in a decade and a half to not be produced by Dieter Dierks and is widely considered to be the last \"classic\" Scorpions album.', 1990, '125,000', 4),
(5, 'Power', 'Power is an EP by American \"Goblin Metal\" band Nekrogoblikon, independently released and produced by Nekrogoblikon themselves in 2013, and was mixed by Matt Hyde. Kerrang! Magazine gave Power 5/5 Stars and Kerrang! Magazine (UK) inserted a poster of \"John Goblikon\" in the August 2013 issue. Power is available on Bandcamp as a digital download and on CD format. A writer for Metal Sucks magazine praised Nekrogoblikon, and wrote an article for the then upcoming release of Power.', 2013, '175,000', 5);

-- --------------------------------------------------------

--
-- Table structure for table `bands`
--

CREATE TABLE `bands` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `origin` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `founded` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `bands`
--

INSERT INTO `bands` (`id`, `name`, `origin`, `founded`, `active`) VALUES
(1, 'Megadeth', 'Los Angeles, California, U.S.', 1983, 1),
(2, 'Red Fang', 'Portland, Oregon, U.S.', 2005, 1),
(3, 'Truckfighters', 'Örebro, Sweden', 2001, 1),
(4, 'Scorpions', 'Hanover, West Germany', 1965, 1),
(5, 'Nekrogoblikon', 'Santa Barbara, California, U.S.', 2006, 1);

-- --------------------------------------------------------

--
-- Table structure for table `band_genres`
--

CREATE TABLE `band_genres` (
  `id` int(11) NOT NULL,
  `bandId` int(11) NOT NULL,
  `genreId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `band_genres`
--

INSERT INTO `band_genres` (`id`, `bandId`, `genreId`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 1, 3),
(4, 2, 4),
(5, 2, 6),
(6, 3, 2),
(7, 3, 4),
(8, 3, 5),
(9, 3, 7),
(10, 4, 2),
(11, 4, 8),
(12, 4, 9),
(13, 5, 10),
(14, 5, 11),
(15, 5, 12),
(16, 5, 13);

-- --------------------------------------------------------

--
-- Table structure for table `genres`
--

CREATE TABLE `genres` (
  `id` int(11) NOT NULL,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `genres`
--

INSERT INTO `genres` (`id`, `name`) VALUES
(1, 'Thrash metal'),
(2, 'Heavy metal'),
(3, 'Speed metal'),
(4, 'Stoner metal'),
(5, 'Stoner rock'),
(6, 'Sludge metal'),
(7, 'Psychedelic rock'),
(8, 'Hard rock'),
(9, 'Glam metal'),
(10, 'Melodic death metal'),
(11, 'Folk metal'),
(12, 'Comedy metal'),
(13, 'Symphonic power metal');

-- --------------------------------------------------------

--
-- Table structure for table `songs`
--

CREATE TABLE `songs` (
  `id` int(11) NOT NULL,
  `name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `duration` varchar(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `albumId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `songs`
--

INSERT INTO `songs` (`id`, `name`, `duration`, `albumId`) VALUES
(1, 'Holy Wars... The Punishment Due', '6:36', 1),
(2, 'Hangar 18', '5:14', 1),
(3, 'Take No Prisoners', '3:28', 1),
(4, 'Five Magics', '5:42', 1),
(5, 'Poison Was the Cure', '2:58', 1),
(6, 'Lucretia', '3:58', 1),
(7, 'Tornado of Souls', '5:22', 1),
(8, 'Dawn Patrol', '1:50', 1),
(9, 'Rust in Peace... Polaris', '5:36', 1),
(10, 'Prehistoric Dog', '4:28', 2),
(11, 'Reverse Thunder', '3:15', 2),
(12, 'Night Destroyer', '3:12', 2),
(13, 'Humans Remain Human Remains', '6:28', 2),
(14, 'Good to Die', '3:29', 2),
(15, 'Bird on Fire', '3:08', 2),
(16, 'Wings of Fang', '2:49', 2),
(17, 'Sharks', '2:26', 2),
(18, 'Whales and Leeches', '4:21', 2),
(19, 'Witness', '2:37', 2),
(20, 'Desert Cruiser', '7:30', 3),
(21, 'Gargarismo', '5:01', 3),
(22, 'Momentum', '7:28', 3),
(23, 'Freewheelin\'', '4:16', 3),
(24, 'The Deal', '4:10', 3),
(25, 'Superfunk', '5:08', 3),
(26, 'Subfloor', '2:32', 3),
(27, 'Gweedo-Weedo', '5:43', 3),
(28, 'Manhattan Project', '6:26', 3),
(29, 'In Search of (The)', '3:35', 3),
(30, 'Intermission', '2:06', 3),
(31, 'A. Zapruder', '4:54', 3),
(32, 'Altered State (Instrumental)', '8:28', 3),
(33, 'Tease Me Please Me', '4:44', 4),
(34, 'Don\'t Believe Her', '4:55', 4),
(35, 'To Be with You in Heaven', '4:48', 4),
(36, 'Wind of Change', '5:10', 4),
(37, 'Restless Nights', '5:44', 4),
(38, 'Lust or Love', '4:22', 4),
(39, 'Kicks After Sex', '3:49', 4),
(40, 'Hit Between the Eyes', '4:33', 4),
(41, 'Money and Fame', '5:06', 4),
(42, 'Crazy World', '5:08', 4),
(43, 'Send Me an Angel', '4:32', 4),
(44, 'Friends (In Space)', '3:30', 5),
(45, 'Nothing But Crickets', '4:10', 5),
(46, 'Powercore', '3:56', 5),
(47, 'Bells & Whistles', '4:09', 5),
(48, 'Giraffe', '3:19', 5);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `username` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(65) COLLATE utf8mb4_unicode_ci NOT NULL,
  `role` enum('manager') COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `role`) VALUES
(1, 'ivan', 'password', 'manager'),
(2, 'testuser', 'password', 'manager');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`),
  ADD KEY `albums_ibfk_1` (`bandId`);

--
-- Indexes for table `bands`
--
ALTER TABLE `bands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `band_genres`
--
ALTER TABLE `band_genres`
  ADD PRIMARY KEY (`id`),
  ADD KEY `bandId` (`bandId`),
  ADD KEY `genreId` (`genreId`);

--
-- Indexes for table `genres`
--
ALTER TABLE `genres`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `songs`
--
ALTER TABLE `songs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `albumId` (`albumId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `bands`
--
ALTER TABLE `bands`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `band_genres`
--
ALTER TABLE `band_genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `genres`
--
ALTER TABLE `genres`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `songs`
--
ALTER TABLE `songs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=49;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `albums`
--
ALTER TABLE `albums`
  ADD CONSTRAINT `albums_ibfk_1` FOREIGN KEY (`bandId`) REFERENCES `bands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `band_genres`
--
ALTER TABLE `band_genres`
  ADD CONSTRAINT `band_genres_ibfk_1` FOREIGN KEY (`bandId`) REFERENCES `bands` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `band_genres_ibfk_2` FOREIGN KEY (`genreId`) REFERENCES `genres` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `songs`
--
ALTER TABLE `songs`
  ADD CONSTRAINT `songs_ibfk_1` FOREIGN KEY (`albumId`) REFERENCES `albums` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
