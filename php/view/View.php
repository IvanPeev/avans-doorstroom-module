<?php
namespace php\view;

class View {
	private $control;
	private $action;
	private static $data = array(); // associative array

	public function setControl($control) {
		$this->control = $control;
	}

	public function setAction($action) {
		$this->action = $action;
	}

	/**
	 * Gives a key-value pair to the view to be expressed in the template
	 */
	public function set($name, $value) {
		if (!isset(View::$data[$name]) || View::$data[$name] !== $value) {
			self::$data[$name] = $value;
		}
	}

	/**
	 * Determines the URL path using $this->action and $this->control
	 * @return type string of the path of the looked up template
	 */
	private function getTemplate() {
		return str_replace('\\', DIRECTORY_SEPARATOR, BASE_NAMESPACE."view\\tpls\\$this->control\\$this->action.php");
	}

	public function show() {
		\header('Content-Type: text/html; charset=utf-8');

		// prevent that variables conflict by dynamically
		// creating a name that already exists
		// thus the strange variable names here
		foreach (self::$data as $qrxU => $Pqzy) {
			$$qrxU = $Pqzy;
		}
		include_once $this->getTemplate();
	}
}
