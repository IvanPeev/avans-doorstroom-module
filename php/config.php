<?php
const BASE_NAMESPACE = 'prod\\';
const DATA_SOURCE_NAME = 'mysql:dbname=music_collection;host=127.0.0.1;charset=utf8';
const DB_USERNAME = 'root';
const DB_PASSWORD = 'qwerty';
const DEFAULT_ROLE = 'guest';

const REQUEST_SUCCESS = 1;
const REQUEST_FAILURE_DATA_INVALID = 3;
const REQUEST_FAILURE_DATA_INCOMPLETE = 5;
const REQUEST_NOTHING_CHANGED = 7;

const DB_NOT_ACCEPTABLE_DATA = 9;
