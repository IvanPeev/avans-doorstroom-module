<?php
namespace php\controls;

class ControlDispatcher {
	private $control;
	private $action;

	public function __construct($control, $action) {
    $this->control = $control;
    $this->action = $action;
  }

	public function dispatch() {
		$className = BASE_NAMESPACE.'controls\\'.ucFirst($this->control).'Controller';

		if (!class_exists($className)) {
			throw new \php\error\FrameworkException("Controller $className doesn't exist!");
		}

		if (!is_subclass_of($className, '\php\\controls\\AbstractController')) {
			throw new \php\error\FrameworkException("Class $className does not extend from the framework AbstractController. This is mandatory.");
		}

		$controller = new $className($this->control, $this->action);
		$controller->execute();
	}
}
