<?php
namespace php\controls;
use php\error as ERROR;

abstract class AbstractController implements IController {
	protected $control;
	protected $action;
	protected $model;
	protected $view;

	public function __construct($control, $action) {
		$this->control = $control;
		$this->action = $action;
		$this->view = new \php\view\View();
		$modelClassName = BASE_NAMESPACE.'models\\'.ucFirst($this->control)."Model";

		if (!class_exists($modelClassName)) {
			throw new ERROR\FrameworkException("Class $modelClassName doesn't exist!");
		}

		if (!is_subclass_of($modelClassName, 'php\\models\\AbstractModel')) {
			throw new ERROR\FrameworkException("Class $modelClassName does not inherit from the framework AbstractModel");
		}

		$this->model = new $modelClassName($control, $action);
		$userPermission = $this->model->getUserPermission();

		if ($userPermission !== $this->control) {
			$this->model->stopSession();
			$this->view->set('message', "You're doing strange things with this application. Use regular forms.");
			$this->forward('default', DEFAULT_ROLE);
		}
	}

	public function execute() {
		$task = $this->action."Action";
		if (method_exists($this, $task)) {
			$this->$task();
			$this->view->setAction($this->action);
			$this->view->setControl($this->control);
			$this->view->show();
		} else {
			$this->forward('default');
		}
	}

	protected function forward($action, $control = null) {
		$action = strtolower(trim($action));
		
		if (isset($control)) {
			$className = BASE_NAMESPACE.'controls\\'.ucFirst($control).'Controller';

			if (!class_exists($className)) {
				throw new ERROR\FrameworkException("Class $className doesn't exist!");
			}

			if (!is_subclass_of($className, 'php\\controls\\AbstractController')) {
				throw new ERROR\FrameworkException("Class $className does not inherit from the framework AbstractController");
			}

			$controller = new $className($control, $action);
		} else {
			$controller = $this;
			$this->action = $action;
			$this->model->setAction($this->action);
		}
		$controller->execute();
		exit();
	}

	protected function logoutAction() {
		$this->model->stopSession();
		$this->forward('default', 'guest');
	}

	protected abstract function defaultAction();
}
