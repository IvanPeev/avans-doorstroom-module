<?php
namespace php\error;

/**
 * This exception is thrown when the framework doesn't find the necessary class
 * or the requested method doesn't exist.
 */
class FrameworkException extends \Exception {
	public function __construct($message, $code = 0, \Exception $previous = null) {
		parent::__construct($message, $code, $previous);
	}
}
