<?php
namespace php\models;

abstract class AbstractModel {
	protected $control;
	protected $action;
	protected $dbh;

	public function __construct($control, $action) {
		$this->control = $control;
		$this->action = $action;
		$this->dbh = new \PDO(DATA_SOURCE_NAME, DB_USERNAME, DB_PASSWORD);
		$this->dbh->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

		if ($this->control !== DEFAULT_ROLE) {
			$this->startSession();
		} else if ($this->getUser() !== NULL) {
			$this->stopSession();
		}
	}

	public function isPostEmpty() {
		return empty($_POST);
	}

	public function getUser() {
		if (!isset($_SESSION) || !isset($_SESSION['user']) || empty($_SESSION['user'])) {
			return NULL;
		} else {
			return $_SESSION['user'];
		}
	}

	public function getUserPermission() {
		$user = $this->getUser();
		$permission = ($user === null) ? DEFAULT_ROLE : $user->getRole();
		return $permission;
	}

	protected function startSession() {
		if (!isset($_SESSION)) {
			\session_start();
		}
	}

	public function stopSession() {
		if (isset($_SESSION)) {
			$_SESSION = [];
			\session_destroy();
		}
	}

	public function setAction($action) {
		$this->action = $action;
	}

	public function getFormData() {
		return $_POST;
	}
}
