<?php
namespace php\models\db;
use php\error as ERROR;

abstract class Entity {
	public function __call($methodName, $args) {
		$methodType = substr($methodName, 0, 3);
		$property = lcfirst(substr($methodName, 3));
		$numberOfArgs = count($args);

		if (!property_exists($this, $property)) {
			echo $methodName;
			echo $property;
			throw new ERROR\FrameworkException("The property $property does not exist!");
		}

		switch($methodType) {
			case 'get':
				if ($numberOfArgs !== 0) {
					throw new ERROR\FrameworkException("$methodName method accepts no arguments, and it definitely shouldn't accept $numberOfArgs argument(s).");
				}
				return $this->$property;

			case 'set':
				if ($numberOfArgs !== 1) {
					throw new ERROR\FrameworkException("$methodName method does not accept $numberOfArgs argument(s).");
				}
				$this->$property = $args[0];
				break;
			
			default:
				throw new ERROR\FrameworkException("The method $methodName does not exist!");
		}
	}
}
